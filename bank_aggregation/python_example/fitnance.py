#! /usr/bin/python

import requests
import hashlib
import json
import time
import uuid

if __name__ == '__main__':

    base_url = 'https://integration.unnax.com/api/v2/fitnance'
    merchant_id = 'your_api_id'
    api_key = 'your_api_code'

    _client = requests.Session()
    _client.headers.update({
        'content-type': "application/x-www-form-urlencoded"
    })

    bank_id = 999
    username = 'user1'
    passwd = 'passwd1'

    request_code = 'TEST_FITNANCE_PYTHON_{0}'.format(uuid.uuid4())

    precalculated_signature = merchant_id + request_code + api_key

    merchant_signature = hashlib.sha1(precalculated_signature.encode('utf-8')).hexdigest()
    callback_url = None
    callback_ok = None
    callback_ko = None

    url = '/start'

    payload = {
        'merchant_id': merchant_id,
        'merchant_signature': merchant_signature,
        'request_code': request_code,
        'callback_url': callback_url,
        'callback_ok': callback_ok,
        'callback_ko': callback_ko,
    }

    result = _client.post(base_url + url, payload)
    print('Api Fitnance POST start response:')
    print(json.dumps(result.text, indent=4, sort_keys=True))

    # Now we will use Null Bank for example

    url = '/login'

    """
        As we see in the previous response, Null Bank requires 2 parameters:
            - username
            - password

        Another example:
            Santander Customer
                - username
                - password
                - document_type -> can be one of the following list (bso_id_number, bso_sec_card, bso_nie)
    """

    data = {
        'bank_id': bank_id,
        'parameters[username]': username,
        'parameters[password]': passwd,

    }

    result = _client.post(base_url+url, data)
    print('Api Fitnance POST login response:')

    print(json.dumps(result.text, indent=4, sort_keys=True))
    if result.status_code == 400:
        exit()

    # Check for status

    while True:
        url = '/login/status'
        result = _client.get(base_url+url)

        print('Api Fitnance GET login response:')
        result_data = json.loads(result.text)
        data = {
            'status': result.status_code,
            'data': result_data
        }
        print(json.dumps(result.text, indent=4, sort_keys=True))

        if result.status_code in (200, 400):
            break

        time.sleep(1)


    # Finish Process
    url = '/complete'

    result = _client.post(base_url + url)

    print("Complete POST")
    print(json.dumps(result.text, indent=4, sort_keys=True))

    time.sleep(6)


    # get data
    url = '/request'
    data = {
        'request_code': request_code,
        'merchant_id': merchant_id,
        'merchant_signature': merchant_signature
    }

    result = _client.post(base_url + url, data)

    print("Result Data")
    print(json.dumps(result.text, indent=4, sort_keys=True))
