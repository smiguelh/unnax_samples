import { decryptFitnance } from './utils';
import { Webhook as WebhookCode } from './codes';


function Token(api_id, api_code, url="https://www.unnax.com") {
    const buff = new Buffer(`${api_id}:${api_code}`),
        tokenB64 = buff.toString('base64');
    return {
        baseUrl: url,
        headers: {
            "Authorization": `Unnax ${tokenB64}`
        },
        api_code: api_code,
        api_id: api_id
    }
}


function Callback(callbackRaw, token) {
    return {
        json: ()=> {
            if(callbackRaw['triggered_event'] == WebhookCode.FITNANCE_READ) {
                return decryptFitnance(callbackRaw['data'], token.api_code, token.api_id);
            }

            if(callbackRaw['triggered_event'] == WebhookCode.EVENT_CREDENTIAL_TOKEN_CREATION) {
                //TODO: validate event signature
                return callbackRaw['data'];
            }
            return callbackRaw
        }
    }
}

export {
    Token,
    Callback
};
