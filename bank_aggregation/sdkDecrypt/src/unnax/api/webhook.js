import axios from 'axios';

function Webhook(token, client=axios) {
    return {
        create: async (options) => {
            //TODO validate options
            let data = Object.assign({}, options);
            if(!data.client) {
                data.client = "callback";
            }
            const headers = token.headers,
                method = "post",
                url = `${token.baseUrl}/api/v3/webhooks/`;

            const response = await client({method, url, data, headers});
            if(response.status !== 201) {
                throw new Error(JSON.stringify(response));
            }
            return response.data.response;
        }
    }
}

export {
    Webhook
};
