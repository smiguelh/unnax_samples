import axios from 'axios';

function TokenizeCredentials(token, client=axios) {
    return {
      init: async (data) => {
        const headers = token.headers,
            method = "post",
            url = `${token.baseUrl}/api/v3/tokenized_credentials/init/`;

        const response = await client({method, url, data, headers});
        if(response.status !== 200) {
            throw new Error(JSON.stringify(response));
        }
        return response.data;
      }
    }
}

export {
    TokenizeCredentials
};
