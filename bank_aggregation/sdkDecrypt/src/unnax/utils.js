import crypto from 'crypto';

const IV_SIZE = 16,
    KEY_SIZE = 16,
    DEFAULT_PADDING = '$';

function strip(text, padding) {
    let textLength = text.length;
    for(let i=text.length - 1; i>0; i--) {
        if(text.charAt(i) !== padding) {
            break;
        }
        textLength = i;
    }
    return text.slice(0, textLength);
}

function decryptFitnance(callbackRaw, api_code, api_id) {
    const iv = api_id.padEnd(IV_SIZE, DEFAULT_PADDING),
        key = api_code.padEnd(KEY_SIZE, DEFAULT_PADDING);

    const decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);
    decipher.setAutoPadding(false);
    const decrypted = decipher.update(callbackRaw, 'base64', 'utf8');

    return JSON.parse(
        strip(decrypted, DEFAULT_PADDING)
    );
}

export {
    decryptFitnance
};
