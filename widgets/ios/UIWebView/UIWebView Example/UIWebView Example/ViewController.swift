//
//  ViewController.swift
//  UIWebView Example
//
//  Created by unnax on 26/7/17.
//  Copyright © 2017 Unnax. All rights reserved.
//

import UIKit
import WebKit


class ViewController: UIViewController, WKScriptMessageHandler {

    @IBOutlet var containerView: UIView? = nil
    
    var webView: WKWebView?
    
    override func loadView() {
        super.loadView();
        
        let contentController = WKUserContentController()
        
        contentController.add( self, name: "Unnax")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        
        self.webView = WKWebView(
            frame: self.containerView!.bounds,
            configuration: config
        )
        self.webView?.scrollView.isScrollEnabled = true
        self.view = self.webView!
        self.view.isUserInteractionEnabled = true
        let url = "YOUR_ENDPOINT_URL"
  
        data_request(api_url: url)
    }
    
    func data_request(api_url: String) {
        /*
         * For this example we call our API directly, but for security reasons you shuld calculate the signature in your backend.
         */
        var request = URLRequest(url: URL(string: api_url)!)
        
        //request this params from your own API or backend application                
        let merchant_id = "YOUR_API_ID"
        let merchant_code = "YOUR_API_CODE"
        let terms = "YOUR_TEMRS_URL"
        let request_code = UUID().uuidString.components(separatedBy: "-")[4]
        var signature = merchant_id + request_code + merchant_code
        
        signature = signature.sha1()
        let paramString = "merchant_signature=" + signature + "&request_code=" + request_code + "&merchant_id=" + merchant_id + "&custom_term_and_conditions_url=" + terms
        print("paramString=\(String(describing: paramString))")
        request.httpMethod = "POST"
        request.httpBody = paramString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            guard let data = data, error == nil else {                                         // check for fundamental networking
                print ("error=\(String(describing: error))")
                return
            }
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {   // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response=\(String(describing: response))")
            }
            let responseString = String(data: data, encoding: .utf8)
            //print("responseString=\(String(describing: responseString))")
            self.webView!.loadHTMLString(responseString!, baseURL: URL(string: api_url)!)
        
        }
        task.resume()

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func userContentController( _ userContentController: WKUserContentController, didReceive message: WKScriptMessage){
        print("Widget msg: \(message.body)")
        let data = (message.body as! String).data(using: String.Encoding.utf8, allowLossyConversion: false)!
        do{
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: String]
            print(json)
            if let step = json["step"] {
                if step ==  "terms"{
                    if let termsUrl = URL(string: json["product"]!){
                        UIApplication.shared.open(termsUrl)
                    }
                    
                }
            }
        }catch let error as NSError{
            print("Failed to load: \(error.localizedDescription)")
        }
        
    }

}

extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0)}
        return hexBytes.joined()
    }
}

